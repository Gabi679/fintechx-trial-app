'use strict';
const bankSearchController = require('./api/controllers/bankSearch.controller');
const SwaggerExpress = require('swagger-express-mw');
const dotenv = require('dotenv');
const path = require('path');
const app = require('express')();
const mongoose = require('mongoose');
const { env } = require('process');

module.exports = app;

const config = {
    appRoot: __dirname,
};

if (process.env.ENV === "dev") {
    const env = dotenv.config({ path: path.resolve(__dirname, (`config/.env.${ process.env.ENV }`)).trim() })
    if (env.error) {
        throw env.error;
    }
}
const envUser = process.env.MONGO_USER;
const envPwd = process.env.MONGO_USER;
let connectionString = '';
if (envUser && envPwd) {
    connectionString = `mongodb://${ process.env.MONGO_USER }:${ process.env.MONGO_PWD }@${ process.env.MONGO_HOST }/${ process.env.MONGO_DB }`;
} else {
    connectionString = `mongodb://${ process.env.MONGO_HOST }/${ process.env.MONGO_DB }`;
}
try {
    mongoose.connect(connectionString, { useNewUrlParser: true, useUnifiedTopology: true}).catch(console.error);
}catch (e) {
    
}

let db = mongoose.connection;

db.on('connecting', function () {
    console.log('connecting to MongoDB...');
});

db.on('error', function (error) {
    console.error('Error in MongoDb connection: ' + error);
    mongoose.disconnect();
});
db.on('connected', function () {
    console.log('MongoDB connected!');
});
db.once('open', function () {
    console.log('MongoDB connection opened!');
});
db.on('reconnected', function () {
    console.log('MongoDB reconnected!');
});
db.on('disconnected', function () {
    console.log('MongoDB disconnected!');
    setTimeout(() => {
        mongoose.connect(connectionString, { useNewUrlParser: true, useUnifiedTopology: true }).catch(console.error);
    }, 3000)
});


SwaggerExpress.create(config, function (err, swaggerExpress) {
    if (err) {
        throw err;
    }
    swaggerExpress.runner.config.swagger.securityHandlers = {
        mySecKey: function (req, authOrSecDef, scopesOrApiKey, callback) {
            // ...
        }
    }

    swaggerExpress.register(app);
    swaggerExpress.runner.config.swagger.securityHandlers ={
        APIKey: function (req, authOrSecDef, scopesOrApiKey, cb) {
            // your security code
            if ('1234' === scopesOrApiKey) {
                cb(true);
            } else {
                cb(new Error('access denied!'));
            }
        }
    };

    const port = process.env.PORT || 3000;
    app.listen(port);
    app.get('/banks', bankSearchController.banks)
});
