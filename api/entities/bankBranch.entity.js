const mongoose = require('mongoose'); 
const { BankType } = require('../enums/bankType');
const { TemperatureUnit } = require('../enums/temperatureUnit');
const { AtmEntitySchemaObject } = require('./atm.entity');
const Schema = mongoose.Schema;

const BankBranchEntitySchema = new Schema({
  ...AtmEntitySchemaObject,
  openingHours: { type: String, required: false },
});

const BankBranchEntityModel = mongoose.model('BankBranch', BankBranchEntitySchema );
module.exports = {
    BankBranchEntityModel,
    BankBranchEntitySchema,
}