const mongoose = require('mongoose'); 
const { AtmEntitySchema } = require('./atm.entity');
const { BankBranchEntitySchema } = require('./bankBranch.entity');
const Schema = mongoose.Schema;

const SearchResultEntitySchema = new Schema({
  city: { type: String, required: true },
  bankName: { type: String, required: true },
  bankBranchOrAtm: { type: [AtmEntitySchema], required: true },
}, { timestamps: true });

const SearchResultEntityModel = mongoose.model('SearchResult', SearchResultEntitySchema );
module.exports = {
    SearchResultEntityModel,
}