const mongoose = require('mongoose'); 
const { BankType } = require('../enums/bankType');
const { TemperatureUnit } = require('../enums/temperatureUnit');
const Schema = mongoose.Schema;

const schemaObject = {
  type: { type:String, enum: Object.values(BankType),  required: true },
  address: {
    city: { type: String, required: true },
    street: { type: String, required: true },
    streetNumber: { type: String, required: true },
    postalCode: { type: Number, required: false },
  },
  weatherInformation: {
    temperature: { type: Number, required: true },
    temperatureUnit: { type:String, enum: Object.values(TemperatureUnit),  required: true },
    windSpeed: { type: Number, required: true },
    weatherDescription: { type: String, required: false },
  }
}
const AtmEntitySchema = new Schema(schemaObject);

const AtmEntityModel = mongoose.model('Atm', AtmEntitySchema );

module.exports = {
    AtmEntityModel,
    AtmEntitySchemaObject: schemaObject,
    AtmEntitySchema,
}
