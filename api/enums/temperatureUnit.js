const TemperatureUnit  = {
    CELSIUS:'Celsius',
    FAHRENHEIT:'Fahrenheit',
    KELVIN:'Kelvin',
}
module.exports = {
    TemperatureUnit
}