const { BankType } = require('../enums/bankType');
const osmService = require('../services/geo/openStreetMap.service');
const openWeatherService = require('../services/weather/openWeather.service');
const { AtmEntityModel } = require('../entities/atm.entity');
const { BankBranchEntityModel } = require('../entities/bankBranch.entity');
const { SearchResultEntityModel } = require('../entities/searchResult.entity');
const OpeningHoursFetchError = require('../errors/opening-hours-fetch-error');
const AddressFetchError = require('../errors/address-fetch-error');
const WeatherFetchError = require('../errors/weather-fetch-error');
const AddressCreationError = require('../errors/address-creation-error');

useDb = true;

async function findBankOrAtm(city, bankName) {
    try {
        
        const cacheResult = await SearchResultEntityModel.findOne({
            city,
            bankName,
            createdAt: { $gte: new Date(new Date().getTime() - 60 * 60 * 1000) }
        })
        useDb = true;
        if (cacheResult) {
            return cacheResult.bankBranchOrAtm;
        }
    } catch (e) {
        useDb = false;
        console.error("Error while fetching from cache", e);
    }

    let resultArray = [];

    try {
        const osmResponse = await getOpenStreetMapResult(city, bankName);
        const filteredOsmResponse = osmResponse
            .filter(osmItem => osmItem.osm_type === 'node');
        resultArray = await createAtmAndBankObjects(filteredOsmResponse);
        try {
            if(useDb){
                await SearchResultEntityModel.create({
                    city,
                    bankName,
                    bankBranchOrAtm: resultArray
                });
            }
        } catch (e) {
            console.error("Error while saving to cache", e);
            return resultArray;
        }
    } catch (error) {
        console.error(error); //TODO handle the error properly in the long term.
        if(useDb){
            const cacheResult = await SearchResultEntityModel.findOne({
                city,
                bankName,
            }, null, { sort: { createdAt: -1 } })
            if (cacheResult) {
                return cacheResult.bankBranchOrAtm ;
            }else{
                throw new Error("There is no result in the cache and i cannot reach the internet");
            }
        }else{
            throw new Error("There is no result in the cache and i cannot reach the internet")
        }
       
    }

    return resultArray;
}

async function createAtmAndBankObjects(filteredOsmResponse) {
    const resultArray = [];
    for (let i = 0; i < filteredOsmResponse.length; i++) {
        const osmItem = filteredOsmResponse[i];
        const weather = await getWeatherInformation(osmItem.lan, osmItem.lon)
        if (osmItem.type === 'atm') {
            const atmEntity = await createAtmObject(osmItem, weather);
            try {
                if(useDb){
                    const atmSaveResult = await AtmEntityModel.create(atmEntity);
                    resultArray.push(atmSaveResult);
                }else{
                    resultArray.push(atmEntity);
                    console.error("Error while saving to database");
                }
            } catch (e) {
                resultArray.push(atmEntity);
                console.error("Error while saving to database", e);
            }
        } else if (osmItem.type === 'bank') {
            const bankBranchEntity = await createBankObject(osmItem, weather);
            try {
                if(useDb){
                    const bankBranchSaveResult = await BankBranchEntityModel.create(bankBranchEntity);
                    resultArray.push(bankBranchSaveResult);
                }else{
                    resultArray.push(bankBranchEntity);
                    console.error("Error while saving to database");
                }
            } catch (e) {
                resultArray.push(bankBranchEntity);
                console.error("Error while saving to database", e);
            }
        }
    }
    return resultArray;
}

async function createAtmObject(osmItem, weatherInfo) {
    let address;
    try {
        address = await getAddressObjectFromLanAndLon(osmItem.lat, osmItem.lon);
    } catch (e) {
        throw new AddressCreationError();
    }
    return {
        type: BankType.ATM,
        address,
        weatherInformation: {
            temperature: weatherInfo.main.temp,
            temperatureUnit: process.env.TEMPERATURE_UNIT === 'metric' ? 'Celsius' : process.env.TEMPERATURE_UNIT === 'internal' ? 'Kelvin' : 'Fahrenheit',
            windSpeed: weatherInfo.wind.speed,
            weatherDescription: weatherInfo.weather[0].description,
        }
    }
}

async function createBankObject(osmItem, weatherInfo) {
    let address, openingHours;
    try {
        address = await getAddressObjectFromLanAndLon(osmItem.lat, osmItem.lon);
    } catch (e) {
        throw new AddressCreationError();
    }
    try {
        openingHours = await getOpeningHoursOfPlace(osmItem.place_id)
    } catch (e) {
        throw new OpeningHoursFetchError();
    }
    return {
        type: BankType.BANK_BRANCH,
        address,
        openingHours,
        weatherInformation: {
            temperature: weatherInfo.main.temp,
            temperatureUnit: process.env.TEMPERATURE_UNIT === 'metric' ? 'Celsius' : process.env.TEMPERATURE_UNIT === 'internal' ? 'Kelvin' : 'Fahrenheit',
            windSpeed: weatherInfo.wind.speed,
            weatherDescription: weatherInfo.weather.description,
        }
    }
}

async function getAddressObjectFromLanAndLon(lat, lon) {
    const reverseAddressSearchObject = await osmService.findFullAddressByCoordinates(lat, lon);
    return {
        city: reverseAddressSearchObject.address.city,
        street: reverseAddressSearchObject.address.road,
        streetNumber: reverseAddressSearchObject.address.city,
        postalCode: reverseAddressSearchObject.address.postcode,
    }
}

async function getWeatherInformation(lat, lon) {
    let weather;
    try {
        weather = await openWeatherService.getWeatherInformation(lat, lon);
    } catch (e) {
        throw new WeatherFetchError();
    }
    return weather;
}

async function getOpeningHoursOfPlace(placeId) {
    const placeDetails = await osmService.getDetailsOfPlace(placeId);
    return placeDetails?.extratags?.opening_hours;
}

async function getOpenStreetMapResult(city, bankName) {
    let address;
    try {
        address = await osmService.findBankOrAtmByAddress(city, bankName);
    } catch (e) {
        throw new AddressFetchError();
    }
    return address;
}

module.exports = {
    findBankOrAtm
}
