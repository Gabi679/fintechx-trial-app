const Nominatim = require('nominatim-geocoder')
const geocoder = new Nominatim({},{limit: 100})
const axios = require('axios')

async function findBankOrAtmByAddress(city, bankName){
  return geocoder.search( { q: `${city} ${bankName}` } );
}

async function findFullAddressByCoordinates(lat, lon){
  return geocoder.reverse( { lat, lon} );
}

async function getDetailsOfPlace(placeId){
  return axios.get("https://nominatim.openstreetmap.org/details", { params: {place_id :placeId, format: 'json' } })
}

module.exports = {
    findBankOrAtmByAddress: findBankOrAtmByAddress,
    getDetailsOfPlace: getDetailsOfPlace,
    findFullAddressByCoordinates: findFullAddressByCoordinates,
}