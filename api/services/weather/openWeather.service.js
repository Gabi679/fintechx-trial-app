const weather = require('openweather-apis');
weather.setLang('en');

async function getWeatherInformation(lan, lon){
    weather.setAPPID(process.env.WEATHER_APP_ID);
    weather.setUnits(process.env.TEMPERATURE_UNIT);
    weather.setCoordinate(lan, lon);
    return new Promise((resolve, reject)=>{
        weather.getAllWeather((err, JSONObj)=>{
            if(err){
                reject(err);
            }
            resolve(JSONObj)
        });
    })
  }

module.exports = {
    getWeatherInformation,
}