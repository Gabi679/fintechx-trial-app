class OpeningHoursFetchError extends Error {  
    constructor () {
      super('Opening hours fetching error')
      this.name = this.constructor.name
      Error.captureStackTrace(this, this.constructor);
    }
  }
  
  module.exports = OpeningHoursFetchError  