class AddressCreationError extends Error {  
    constructor () {
      super('Address Creation Error!')
      this.name = this.constructor.name
      Error.captureStackTrace(this, this.constructor);
    }
  }
  
  module.exports = AddressCreationError  