class WeatherFetchError extends Error {  
    constructor () {
      super('Weather fetch error')
      this.name = this.constructor.name
      Error.captureStackTrace(this, this.constructor);
    }
  }
  
  module.exports = WeatherFetchError  