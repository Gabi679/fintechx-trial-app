class AddressFetchError extends Error {  
    constructor () {
      super('Address fetch error')
      this.name = this.constructor.name
      Error.captureStackTrace(this, this.constructor);
    }
  }
  
  module.exports = AddressFetchError  