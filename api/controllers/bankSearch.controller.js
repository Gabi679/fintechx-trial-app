'use strict';
const bankSearchService = require('../services/bankSearch.service');

async function banks(req, res) {
  const city = req.swagger.params.city.value;
  const bank = req.swagger.params.bankName.value;
  try {
    const response = await bankSearchService.findBankOrAtm(city, bank);
    res.json(response);
  } catch (error) {
   res.status(500).send(error.message);
  }

}


module.exports = {
  banks: banks
};
